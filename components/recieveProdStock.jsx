import React, { Component } from "react";
class RecieveProdStock extends Component {
  state = {
    codes: [],
    stock: { code: "", quantity: "" },
  };
  makeCode = (arr) => {
    let s1 = { ...this.state };
    for (let i = 0; i < arr.length; i++) {
      s1.codes.push(arr[i].code);
    }
    this.setState(s1);
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    s1.stock[input.name] = input.value;
    this.setState(s1);
  };
  handleSubmit = (e) => {
    e.preventDefault();
    console.log("Handle Submit", this.state.stock);
    let { code, quantity } = this.state.stock;
    if (!code) {
      alert("Select the Product code");
    } else if (!quantity) {
      alert("Enter the Quantity");
    } else if (code && quantity) this.props.onSubmit(this.state.stock);
  };
  render() {
    let { products } = this.props;
    let { codes, stock } = this.state;
    if (codes.length === 0) this.makeCode(products);
    let { code, quantity } = stock;
    return (
      <div className="container">
        <h4 className="mt-3">
          Select the product whose stocks have been received
        </h4>
        <div className="form-group">
          <select
            name="code"
            className="form-control"
            value={code}
            onChange={this.handleChange}
          >
            <option disabled value="">
              Select Product Code
            </option>
            {codes.map((b1) => (
              <option>{b1}</option>
            ))}
          </select>
        </div>
        <div className="form-group">
          <h5>Stock Received</h5>
          <input
            type="number"
            className="form-control"
            name="quantity"
            value={quantity}
            placeholder="Enter Quantity"
            onChange={this.handleChange}
          />
        </div>
        <button className="btn btn-primary mt-3" onClick={this.handleSubmit}>
          Submit
        </button>
        <br />
        <button
          className="btn btn-primary mt-3"
          onClick={() => this.props.onView(0)}
        >
          Go Back to Home Page
        </button>
      </div>
    );
  }
}
export default RecieveProdStock;
