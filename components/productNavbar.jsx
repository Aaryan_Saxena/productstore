import React, { Component } from "react";
class ProductNavbar extends Component {
  render() {
    const { products, quantity, value } = this.props;
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
          <a href="#" className="navbar-brand">
            ProdStoreSys
          </a>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a href="#" className="nav-link">
                  Products
                  <span className="badge badge-pill badge-secondary">
                    {products}
                  </span>
                </a>
              </li>
              <li className="nav-item">
                <a href="#" className="nav-link">
                  Quantity
                  <span className="badge badge-pill badge-secondary">
                    {quantity}
                  </span>
                </a>
              </li>
              <li className="nav-item">
                <a href="#" className="nav-link">
                  Value
                  <span className="badge badge-pill badge-secondary">
                    {value}
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}
export default ProductNavbar;
