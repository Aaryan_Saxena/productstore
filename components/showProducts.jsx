import React, { Component } from "react";
class ShowProducts extends Component {
  render() {
    const { products, onView, editIndex } = this.props;
    return (
      <div className="container">
        <div className="row mt-4">
          {products.map((pr, index) => {
            let {
              code,
              price,
              brand,
              category,
              specialOffer,
              limitedStock,
              quantity,
            } = pr;
            return (
              <React.Fragment>
                <div className="col-3 border text-center bg-light">
                  <h5>Code : {code}</h5>
                  <p className="mb-0">
                    Brand : {brand}
                    <br />
                    Category : {category}
                    <br />
                    Price : {price}
                    <br />
                    Quantity : {quantity ? quantity : 0}
                    <br />
                    Special Offers : {specialOffer ? "Yes" : "NO"}
                    <br />
                    Limited Stocks : {limitedStock ? "Yes" : "NO"}
                  </p>
                  <button
                    className="btn btn-warning mt-0"
                    onClick={() => editIndex(index)}
                  >
                    Edit Details
                  </button>
                </div>
              </React.Fragment>
            );
          })}
        </div>
        <button className="btn btn-primary ml-2 mt-4" onClick={() => onView(1)}>
          Add New Product
        </button>
        <button className="btn btn-primary ml-2 mt-4" onClick={() => onView(2)}>
          Receive Stocks
        </button>
      </div>
    );
  }
}
export default ShowProducts;
