import React, { Component } from "react";
import ProductNavbar from "./productNavbar";
import ShowProducts from "./showProducts";
import AddProduct from "./addProduct";
import RecieveProdStock from "./recieveProdStock";
class ProductsComp extends Component {
  state = {
    products: [
      {
        code: "PEP1253",
        price: 20,
        brand: "Pepsi",
        category: "Food",
        specialOffer: false,
        limitedStock: false,
        quantity: 25,
      },
      {
        code: "MAGG021",
        price: 25,
        brand: "Nestle",
        category: "Food",
        specialOffer: true,
        limitedStock: true,
        quantity: 10,
      },
      {
        code: "LEV501",
        price: 1000,
        brand: "Levis",
        category: "Apparel",
        specialOffer: true,
        limitedStock: true,
        quantity: 3,
      },
      {
        code: "CLG281",
        price: 60,
        brand: "Colgate",
        category: "Personal	Care",
        specialOffer: true,
        limitedStock: true,
        quantity: 5,
      },
      {
        code: "MAGG451",
        price: 25,
        brand: "Nestle",
        category: "Food",
        specialOffer: true,
        limitedStock: true,
        quantity: 0,
      },
      {
        code: "PAR250",
        price: 40,
        brand: "Parachute",
        category: "Personal	Care",
        specialOffer: true,
        limitedStock: true,
        quantity: 5,
      },
    ],
    view: 0,
    editIndex: -1,
  };
  handleView = (num) => {
    let s1 = { ...this.state };
    s1.view = num;
    s1.editIndex = -1;
    this.setState(s1);
  };
  handleSubmit = (product) => {
    let s1 = { ...this.state };
    s1.editIndex >= 0
      ? (s1.products[s1.editIndex] = product)
      : s1.products.push(product);
    s1.editIndex = -1;
    s1.view = 0;
    this.setState(s1);
  };
  handleEdit = (index) => {
    let s1 = { ...this.state };
    s1.editIndex = index;
    s1.view = 1;
    this.setState(s1);
  };
  handleStock = (stock) => {
    let s1 = { ...this.state };
    let find = s1.products.find((pr) => pr.code === stock.code);
    find.quantity = stock.quantity;
    s1.view = 0;
    this.setState(s1);
  };
  render() {
    const { products, brands, view, editIndex } = this.state;
    let qty = products.reduce((acc, curr) => (acc = acc + +curr.quantity), 0);
    let value = products.reduce(
      (acc, curr) => (acc = acc + +curr.quantity * +curr.price),
      0
    );
    let product = {
      code: "",
      price: "",
      category: "",
      brand: "",
      specialOffer: "",
      limitedStock: "",
    };
    return (
      <React.Fragment>
        <ProductNavbar
          products={products.length}
          quantity={qty}
          value={value}
        />
        {view === 0 ? (
          <ShowProducts
            products={products}
            onView={this.handleView}
            editIndex={this.handleEdit}
          />
        ) : view === 1 ? (
          <AddProduct
            product={editIndex >= 0 ? products[editIndex] : product}
            onSubmit={this.handleSubmit}
            edit={editIndex >= 0}
            onView={this.handleView}
          />
        ) : view === 2 ? (
          <RecieveProdStock
            products={products}
            onSubmit={this.handleStock}
            onView={this.handleView}
          />
        ) : (
          ""
        )}
      </React.Fragment>
    );
  }
}
export default ProductsComp;
