import React, { Component } from "react";
class AddProduct extends Component {
  state = {
    product: this.props.product,
    categories: ["Food", "Personal Care", "Apparel"],
    foodbrands: [
      "Nestle",
      "Haldiram",
      "Pepsi",
      "Coca	Cola",
      "Britannia",
      "Cadburys",
      "P&G",
    ],
    personalBrands: ["Colgate", "Parachute", "Gillete", "Dove"],
    apparelBrands: ["Levis", "Van	Heusen", "Manyavaar", "Zara"],
    errors: {},
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    input.type === "checkbox"
      ? (s1.product[input.name] = input.checked)
      : input.name === "code"
      ? (s1.product[input.name] = input.value.toUpperCase())
      : (s1.product[input.name] = input.value);
    this.setState(s1);
  };
  handleSubmit = (e) => {
    e.preventDefault();
    console.log("Handle Submit", this.state.product);
    let product = this.props.edit
      ? { ...this.state.product }
      : { ...this.state.product, quantity: 0 };
    let errors = this.validateAll();
    if (this.isvalid(errors)) this.props.onSubmit(product);
    else {
      let s1 = { ...this.state };
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isvalid = (errors) => {
    console.log(errors);
    let keys = Object.keys(errors);
    console.log(keys);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  validateAll = () => {
    let { code, price, category, brand } = this.state.product;
    let errors = {};
    errors.code = this.validateCode(code);
    errors.price = this.validatePrice(price);
    errors.category = this.validateCategory(category);
    if (category) {
      errors.brand = this.validateBrand(brand);
    }
    return errors;
  };
  validateCode = (code) => (!code ? "Product code is required" :"");
  validatePrice = (price) =>
    !price
      ? "Price is required"
      : price < 0
      ? "Price Should be greater than 0"
      : "";
  validateCategory = (category) =>
    !category ? alert("Category is required") : "";
  validateBrand = (brand) => (!brand ? alert("Brand is required") : "");
  render() {
    let {
      code,
      price,
      category,
      brand,
      specialOffer,
      limitedStock,
    } = this.state.product;
    let {
      errors,
      categories,
      foodbrands,
      apparelBrands,
      personalBrands,
    } = this.state;
    let brands =
      category === "Food"
        ? [...foodbrands]
        : category === "Apparel"
        ? [...apparelBrands]
        : category === "Personal Care"
        ? [...personalBrands]
        : [];
    let readOnly = this.props.edit ? "readOnly" : "";
    return (
      <div className="container">
        <div className="form-group mt-4">
          <h5>Product Code</h5>
          <input
            type="text"
            className="form-control"
            id="code"
            name="code"
            value={code}
            placeholder="Enter Product Code"
            onChange={this.handleChange}
            readOnly={readOnly}
          />
          {errors.code ? (
            <input
              type="text"
              className="form-control text-danger"
              value={errors.code}
              disabled
              readOnly
            />
          ) : (
            ""
          )}
        </div>
        <div className="form-group">
          <h5>Price</h5>
          <input
            type="number"
            className="form-control"
            id="price"
            name="price"
            value={price}
            placeholder="Enter Price"
            onChange={this.handleChange}
          />
          {errors.price ? (
            <input
              type="text"
              className="form-control text-danger"
              value={errors.price}
              disabled
              readOnly
            />
          ) : (
            ""
          )}
        </div>
        {this.showRadio(
          "Category",
          categories,
          "category",
          category,
          errors.category
        )}
        <br />
        <div className="form-group">
          <select
            name="brand"
            className="form-control"
            value={brand}
            onChange={this.handleChange}
          >
            <option disabled value="">
              Select Brand
            </option>
            {brands.map((b1) => (
              <option>{b1}</option>
            ))}
          </select>
        </div>
        <label className="form-check-label font-weight-bold">
          Choose other info about the product
        </label>
        <div className="form-check">
          <input
            className="form-check-input"
            type="checkbox"
            name="specialOffer"
            value={specialOffer}
            checked={specialOffer}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Special Offer</label>
        </div>
        <div className="form-check">
          <input
            className="form-check-input"
            type="checkbox"
            name="limitedStock"
            value={limitedStock}
            checked={limitedStock}
            onChange={this.handleChange}
          />
          <label className="form-check-label">Limited Stock</label>
        </div>
        <button className="btn btn-primary" onClick={this.handleSubmit}>
          {this.props.edit ? "Edit Product" : "Add Product"}
        </button>
        <br />
        <button
          className="btn btn-primary mt-3"
          onClick={() => this.props.onView(0)}
        >
          {" "}
          Go Back to Home Page
        </button>
      </div>
    );
  }
  showRadio = (label, arr, name, selOpt, errors) => {
    return (
      <React.Fragment>
        <label className="form-check-label font-weight-bold">{label}</label>
        <br />
        {arr.map((opt) => {
          return (
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name={name}
                value={opt}
                checked={selOpt === opt}
                onChange={this.handleChange}
              />
              <label className="form-check-label">{opt}</label>
            </div>
          );
        })}
        {errors ? (
          <span className="text-danger">
            <br />
            {errors}
          </span>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  };
}
export default AddProduct;
